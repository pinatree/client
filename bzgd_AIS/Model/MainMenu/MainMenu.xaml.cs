﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Windows.Interactivity;

using bzgd_AIS.Model.GlobalControls;

using bzgd_AIS.Model.Helpers;
using bzgd_AIS.Model.Helpers.ChildClasses;
using bzgd_AIS.Model.Helpers.StaticFunctions;

using System.Collections.ObjectModel;

using bzgd_AIS.Model.GlobalControls.RequestPanel;

using bzgd_AIS.Model.MainMenu.Pages;

using bzgd_dr.WCF.Client;
using bzgd_dr.EntityFramework.DataTypes;
using bzgd_dr.WCF.ConnectionDataTypes;

using System.IO;

namespace bzgd_AIS.Model.MainMenu
{    
    public partial class MainMenu : Window
    {
        public MainMenu()
		{
			InitializeComponent();
		}

		//bzgd_AIS.Model.GlobalControls.TrayMenu trayMenu;
		//public void InitalizeIcon()
		//{
		//	trayMenu = new bzgd_AIS.Model.GlobalControls.TrayMenu(this);

		//	this.ShowInTaskbar = true;
		//	System.Windows.Forms.NotifyIcon ni = new System.Windows.Forms.NotifyIcon();
		//	ni.Icon = new System.Drawing.Icon(@"D:\Diplom\Resourses\trayIco.ico");
		//	ni.ContextMenu = new System.Windows.Forms.ContextMenu();

		//	ni.Visible = true;
		//	ni.Click +=
		//		delegate (object x1, EventArgs x2)
		//		{
		//			trayMenu.Left = System.Windows.Forms.Cursor.Position.X - trayMenu.Width;
		//			trayMenu.Top = System.Windows.Forms.Cursor.Position.Y - trayMenu.Height;
		//			trayMenu.Show();
		//		};
		//}

		//void OnRequestClick(ShortRequestWithAttachments request)
  //      {
  //          try
  //          {
  //              new RequestDetailsForm(request, (x) => { DownloadAttachment(x); }).ShowDialog();
  //          }
  //          catch(Exception ex)
  //          {
  //              MessageBox.Show("Задание больше не существует в системе. Возможно, оно было удалено администратором. Описание:\n" + ex.Message);
  //              return;
  //          }
  //      }

		//public void RemoveAttachmentData(ShortAttachmentData attachment)
		//{
		//	//contract.RemoveAttachmentData(attachment);
		//}
		

		//public void UpdateRequestsList(ObservableCollection<RequestPanel> requests)
		//{
		//	this.ActualRequests = requests;
		//	this.tasksPage.RequestsItemsControl.ItemsSource = ActualRequests;
		//}
		//public void InitalizeMenuButtons()
  //      {
  //          MenuButtons =  new ObservableCollection<MenuButton>
  //          {
  //              new MenuButton((object x) =>
  //              {
		//			UpdateRequestsList(new ObservableCollection<RequestPanel>(GetRequestsList(new RequestsSearchFilter())));
  //              },
  //              "Выданные задания", @"D:\Diplom\Resourses\gear_icon.png"),
  //              new MenuButton((object x) => { }, "Создать задание", @"D:\Diplom\Resourses\strelka_right.png"),
  //              new MenuButton((object x) =>
  //              {
		//			UpdateRequestsList(new ObservableCollection<RequestPanel>(GetRequestsList(new RequestsSearchFilter(){ Overdue = true })));
  //              }, "Просроченные", @"D:\Diplom\Resourses\strelka_right.png"),
  //              new MenuButton((object x) => { }, "История заданий", @"D:\Diplom\Resourses\gear_icon.png"),
  //              new MenuButton((object x) => { new WorkingConfigurationWindow().ShowDialog(); }, "Параметры", @"D:\Diplom\Resourses\strelka_right.png"),
  //              new MenuButton((object x) =>
  //              {
		//			UpdateRequestsList(new ObservableCollection<RequestPanel>(GetRequestsList(new RequestsSearchFilter(){ Reciever = contract.myLogin })));
  //              }, "Ваши задания", @"D:\Diplom\Resourses\gear_icon.png")
  //          };
  //      }

		//public void DownloadAttachment(int id)
		//{
		//	MessageBox.Show("downloading! " + id);

		//	System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

		//	if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
		//		return;
			
		//	AttachmentData result = contract.GetAttachment(id);
		//	MessageBox.Show(result.FileName);
		//	FileStream stream = new FileStream(dialog.SelectedPath + @"\" + result.FileName, FileMode.CreateNew, FileAccess.ReadWrite);
		//	stream.Write(result.content, 0, result.content.Length);
		//}

  //      public List<RequestPanel> GetRequestsList(RequestsSearchFilter filter)
  //      {
  //          ShortRequestWithAttachmentsList requestsFromContractList = contract.GetShortRequests(filter);

  //          List<ShortRequestWithAttachments> requestsFromContract = requestsFromContractList.ShortRequestWithAttachments;

  //          var result = requestsFromContract.Select((x) =>
  //          {
  //              return new RequestPanel(x, OnRequestClick);
  //          });

  //          return result.ToList();
  //      }
    }
}
