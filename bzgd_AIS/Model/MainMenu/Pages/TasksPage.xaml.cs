﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Collections.ObjectModel;

using Xceed.Wpf.Toolkit;

using bzgd_AIS.Model.GlobalControls;

using bzgd_AIS.Model.GlobalControls.RequestPanel;

namespace bzgd_AIS.Model.MainMenu.Pages
{
    /// <summary>
    /// Логика взаимодействия для TasksPage.xaml
    /// </summary>
    public partial class TasksPage : Page
    {
        public TasksPage()
        {
            InitializeComponent();
			//RequestsItemsControl.GetBindingExpression(new DependencyProperty()).UpdateTarget();
        }


		private void DateFromCalendarSelect(object sender, RoutedEventArgs e)
		{
			CalendarRetDate calendar = new CalendarRetDate();
			calendar.ShowDialog();
			if (calendar.DialogResult == false || calendar.AnswerDate == null)
			{
				return;
			}

			DateTime dateTime = (DateTime)(calendar.AnswerDate);

			this.MaskedTB_FROM.Text = ToFourOrMoreSymbols(dateTime.Year.ToString()) +
				ToTwoOrMoreSymbols(dateTime.Month.ToString()) +
				ToTwoOrMoreSymbols(dateTime.Day.ToString()) +
				ToTwoOrMoreSymbols(dateTime.Hour.ToString()) +
				ToTwoOrMoreSymbols(dateTime.Minute.ToString());
		}

		private void DateToCalendarSelect(object sender, RoutedEventArgs e)
		{
			CalendarRetDate calendar = new CalendarRetDate();
			calendar.ShowDialog();
			if (calendar.DialogResult == false || calendar.AnswerDate == null)
			{
				return;
			}

			DateTime dateTime = (DateTime)(calendar.AnswerDate);

			this.MaskedTB_TO.Text = FormattedDateTime(dateTime);
		}

		private string FormattedDateTime(DateTime dateTime)
		{
			return ToFourOrMoreSymbols(dateTime.Year.ToString()) +
				ToTwoOrMoreSymbols(dateTime.Month.ToString()) +
				ToTwoOrMoreSymbols(dateTime.Day.ToString()) +
				ToTwoOrMoreSymbols(dateTime.Hour.ToString()) +
				ToTwoOrMoreSymbols(dateTime.Minute.ToString());
		}

		private string ToFourOrMoreSymbols(string val)
		{
			switch (val.Length)
			{
				case (0):
					{
						return "0000";
					}
				case (1):
					{
						return "000" + val;
					}
				case (2):
					{
						return "00" + val;
					}
				case (3):
					{
						return "0" + val;
					}
				default:
					{
						return val;
					}
			}
		}

		private string ToTwoOrMoreSymbols(string val)
		{
			switch (val.Length)
			{
				case (0):
					{
						return "00";
					}
				case (1):
					{
						return "0" + val;
					}
				default:
					{
						return val;
					}
			}
		}
	}
}
