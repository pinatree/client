﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using bzgd_dr.WCF.Client;
using bzgd_dr.EntityFramework.DataTypes;
using bzgd_dr.WCF.ConnectionDataTypes;

using System.Collections.ObjectModel;

namespace bzgd_AIS.Model.GlobalControls
{
    public partial class RequestDetailsForm : Window
	{
		public RequestDetailsForm(ShortRequestWithAttachments shortRequestWithAttachments, int selectedIndex, bool RequestForUser)
		{
			InitializeComponent();

			this.CompletionStage.SelectedIndex = selectedIndex;
			if (RequestForUser)
			{
				this.AddAttachmentButton.Visibility = Visibility.Hidden;
				this.CompletionStage.IsEnabled = false;
				this.RemoveRequestButton.Visibility = Visibility.Hidden;
				this.MessageTextBox.IsReadOnly = true;
			}
			
			this.Resources.Add("this", this);
			this.SenderTB.Text = shortRequestWithAttachments.Request.login_senderId;
			this.RecieverTB.Text = shortRequestWithAttachments.Request.login_recieverId;
			this.MessageTextBox.Text = shortRequestWithAttachments.Request.recourse;
		}
    }
}
