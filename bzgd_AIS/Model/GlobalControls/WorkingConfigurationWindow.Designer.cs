﻿namespace bzgd_AIS.Model.GlobalControls
{
    partial class WorkingConfigurationWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AutoRunChB = new System.Windows.Forms.CheckBox();
            this.HideIntoTrayChB = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SaveConfiguration = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AutoRunChB
            // 
            this.AutoRunChB.AutoSize = true;
            this.AutoRunChB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AutoRunChB.Location = new System.Drawing.Point(222, 53);
            this.AutoRunChB.Name = "AutoRunChB";
            this.AutoRunChB.Size = new System.Drawing.Size(15, 14);
            this.AutoRunChB.TabIndex = 17;
            this.AutoRunChB.UseVisualStyleBackColor = true;
            // 
            // HideIntoTrayChB
            // 
            this.HideIntoTrayChB.AutoSize = true;
            this.HideIntoTrayChB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HideIntoTrayChB.Location = new System.Drawing.Point(168, 11);
            this.HideIntoTrayChB.Name = "HideIntoTrayChB";
            this.HideIntoTrayChB.Size = new System.Drawing.Size(15, 14);
            this.HideIntoTrayChB.TabIndex = 16;
            this.HideIntoTrayChB.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(204, 16);
            this.label4.TabIndex = 13;
            this.label4.Text = "Автозапуск вместе с Windows";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(150, 16);
            this.label3.TabIndex = 12;
            this.label3.Text = "Сворачивание в трей";
            // 
            // SaveConfiguration
            // 
            this.SaveConfiguration.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SaveConfiguration.Location = new System.Drawing.Point(15, 88);
            this.SaveConfiguration.Name = "SaveConfiguration";
            this.SaveConfiguration.Size = new System.Drawing.Size(250, 50);
            this.SaveConfiguration.TabIndex = 9;
            this.SaveConfiguration.Text = "Сохранить";
            this.SaveConfiguration.UseVisualStyleBackColor = true;
            this.SaveConfiguration.Click += new System.EventHandler(this.SaveConfiguration_Click);
            // 
            // WorkingConfigurationWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(284, 147);
            this.Controls.Add(this.AutoRunChB);
            this.Controls.Add(this.HideIntoTrayChB);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.SaveConfiguration);
            this.Name = "WorkingConfigurationWindow";
            this.Text = "WorkingConfigurationWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox AutoRunChB;
        private System.Windows.Forms.CheckBox HideIntoTrayChB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button SaveConfiguration;
    }
}